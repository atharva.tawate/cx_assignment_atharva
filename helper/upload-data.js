"use strict";

const xlsx = require("xlsx");
const firestore = require("./../lib/database/firestore-connector");

const workbook = xlsx.readFile("data.xlsx");
const sheet = workbook.SheetNames;
const jsonObject = xlsx.utils.sheet_to_json(workbook.Sheets[sheet[0]]);
const db = await firestore();
jsonObject.forEach(json => {
    db.collection("accont").add(json).then(() => {
        console.log(json.account_no);
    }).catch(err => {
        console.log(err);
    });
});