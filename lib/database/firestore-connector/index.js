"use strict";

const { Firestore } = require("@google-cloud/firestore");


// const connectionTest = async (firestore) => {
//     console.log(firestore);
//     const connection = await firestore.collection("accont").get();
//     if (!connection.empty) {
//         connection.forEach(element => {
//             console.log(element.data());
//         });
//     }
//     return;
// };

module.exports = async () => {
    const firestore = new Firestore({
        projectId: "bank-ass-y9ap",
        keyFilename: "keys/service-account.json"
    });
    // await connectionTest(firestore);
    return firestore;
};
