/**
 * Copyright 2020 Quantiphi, Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 "use strict";

 /**
  * Default tag controller
  * @param {object} df webhook fulfillment object
  */
const firestore = require("../../../lib/database/firestore-connector");
 
const defaultTag= async (df) =>{
    const db = await firestore();
    let parameters = df._request.sessionInfo.parameters;
    let accountNo = parseInt(parameters.account_no);
    let ssnNo = parseInt(parameters.ssn_no);
    await db.collection("accont").where("account_no", "==", accountNo).
        where("ssn_no", "==", ssnNo).get().then(snapShot => {
            let account = snapShot.docs[0].data();
            console.log(account.amount);
            df.setResponseText(`Thank you for confirming! You currently have $${account.amount} in your account.`);
        })
 };
 
 module.exports = defaultTag;
 